const sharp = require('sharp');
const path = require('path');
const uuidv4 = require('uuid/v4');

class Resize {
  constructor(folder) {
    this.folder = folder;
  }
  async save(buffer) {
    const filename = Resize.filename();
    const filepath = this.filepath(filename);

    await sharp(buffer)
      .resize(800, 300, {
        fit: sharp.fit.inside,
        withoutEnlargement: true
      })
      .toFile(filepath);
    
    return filename;
  }
  static filename() {
    return `${uuidv4()}.png`;
  }
  filepath(filename) {
    console.log(`${uuidv4()}.png`)
    console.log(`${this.folder}`+ "\\" +`${filename}`)
    return path.resolve(`${this.folder}/${filename}`)
  }
}
module.exports = Resize;