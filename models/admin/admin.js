const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let adminSchema = new Schema ({
    name : {type: String},
    email : {type: String},
    level : {type: String},
    status : {type: String},
    createdAt : {type: String},
    password : {type: String},
    salt : {type: String},
})

module.exports = mongoose.model('admin', adminSchema);