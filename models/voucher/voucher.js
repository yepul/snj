const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let voucherSchema = new Schema ({
    code : {type: String}, 
    title : {type: String},
    subtitle : {type: String},
    content : {type: String},
    cost : {type : Number},
    status : {type: String},
    createdAt : {type: String},
    redeemBy : {type: String},
    expiredAt : {type: String},
    imageSrc : {type: String, default : "https://sjfrederick.co.uk/wp-content/uploads/2017/10/gift-voucher-prod-image.png"},
    sortOrder : {type: String},
})

module.exports = mongoose.model('voucher', voucherSchema);