const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let fbAccountKitSchema = new Schema ({
    authorizationAccessToken : {type: String},
})

module.exports = mongoose.model('fbAccountKit', fbAccountKitSchema);