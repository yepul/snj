const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userSchema = new Schema ({
    userId : {type: String},
    phoneNumber : {type: String},
    verificationCode : {type: String}, 
    firstName : {type: String},  
    lastName : {type: String},  
    email : {type: String},
    codeExpired : {type: String},
    ic : {type: String},
    dob : {type: String},
    gender : {type: String},

    maritalStatus : {type: String},
    
    race : {type: String},

    address : {type: String},
    street : {type: String},
    city : {type: String},
    state : {type: String},
    zipcode : {type: String},
    country : {type: String},

    occupation : {type: String},

    alternateLookup : {type: String},
    code : {type: String},

    status : {type: String},
    createdAt : {type: String},
    LastLogin : {type: String},
    password : {type: String},
})

module.exports = mongoose.model('user', userSchema);