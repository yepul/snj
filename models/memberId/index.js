const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let memberIdSchema = new Schema ({
    type : {type: String},
    memberId : {type: Number},
})

module.exports = mongoose.model('memberId', memberIdSchema);