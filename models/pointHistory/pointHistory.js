const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let newsSchema = new Schema ({
    title : {type: String},
    subtitle : {type: String},
    content : {type: String},
    slider : {type: String},
    status : {type: String},
    publishedAt : {type: String},
    expiredAt : {type: String},
    imageSrc : {type: String},
    sortOrder : {type: String},
})

module.exports = mongoose.model('news', newsSchema);