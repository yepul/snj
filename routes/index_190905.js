const express = require('express');
const bcrypt = require('bcrypt');
const moment = require('moment')

let app = express();
let News = require('../models/news/news');
let User = require('../models/user/user');
let Admin = require('../models/admin/admin');
// let PointHistory = require('../models/pointHistory/pointHistory');

app.get('/',(req,res)=>{
    res.redirect('/dashboard');
})

// app.post('/dashboard',(req,res)=>{
//     console.log('dashboard : /')
//     // News.find().exec((err, news)=>{
//         // console.log(news)
//         res.render('modules/dashboard/dashboard',{news : ""});
//     // })
// })

app.get('/login',(req,res)=>{
    console.log('login : /')
    // News.find().exec((err, news)=>{
        // console.log(news)
        res.render('modules/login/index',{news : ""});
    // })
})

app.get('/news',(req,res)=>{
    console.log('news : /')
    News.find().exec((err, news)=>{
        // console.log(news)
        res.render('modules/news/news',{news : news});
    })
})

app.post('/updateNews',(req,res)=>{
    console.log('news : /')
    News.find().exec((err, news)=>{
        // console.log(news)
        res.render('modules/news/news',{news : news});
    })
})

app.get('/newsDetails',(req,res)=>{
    if(req.query.newsId){
        let newsId = req.query.newsId
        console.log('newsDetails : /' + newsId)
        News.find({_id : newsId}).exec((err, news)=>{
            // console.log(news)
            res.render('modules/news/news_details',
                {news : {
                    _id : news[0]._id,
                    title : news[0].title,
                    subtitle : news[0].subtitle,
                    content : news[0].content,
                    slider : news[0].slider,
                    status : news[0].status,
                    publishedAt : moment(news[0].publishedAt).format('YYYY-MM-DDTHH:mm'),
                    expiredAt : moment(news[0].expiredAt).format('YYYY-MM-DDTHH:mm'),
                    imageSrc : news[0].imageSrc,
                    sortOrder : news[0].sortOrder
                }}
            );
        })
    }else{
        console.log('newsDetailsNew : /')
        res.render('modules/news/news_details',{news : ""});
    }
})

app.post('/updateNewsDetails',(req,res)=>{
    console.log('updateNewsDetails : /' + req.body.id)
    // console.log(req.body.id)
    let update = {}
    req.body.title ? update.title = req.body.title : ""
    req.body.subtitle ? update.subtitle = req.body.subtitle : ""
    req.body.content ? update.content = req.body.content : ""
    req.body.slider ? update.slider = req.body.slider : ""
    req.body.status ? update.status = req.body.status : ""
    req.body.publishedAt ? update.publishedAt = moment(req.body.publishedAt).format('YYYY-MM-DD HH:mm:ss') : ""
    req.body.expiredAt ? update.expiredAt = moment(req.body.expiredAt).format('YYYY-MM-DD HH:mm:ss') : ""

    News.findOneAndUpdate({_id : req.body.id},update).exec((err,done)=>{
        if(done){
            res.redirect('/news');
        }else{
            console.log(err)
        }
    })
})

app.post('/addAdmin', function(req,res,next){
    let plainPassword = req.body.password
    let saltRounds = 10;
    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(plainPassword, salt);

    let newAdmin = new Admin({
        name : res.body.username,
        email : res.body.email,
        level : "admin",
        status : "active",
        createdAt : moment().unix(),
        password : hash,
    })

    newAdmin.save((err, done) =>{
        res.json({status : 200})
    })        
})

app.post('/loginAdmin', function(req,res,next){
    let name = req.body.username
    let password = req.body.password

    Admin.findOne({name : name}).exec((err, admin)=>{
        if(bcrypt.compareSync(password, admin.password)){
            res.redirect('/dashboard');
        }else{
            res.json({status : 400})
        }
    })

})

app.get('/admin',(req,res)=>{
    console.log('admin : /')
    Admin.find({},{name : 1, email : 1, level : 1, status : 1, createdAt : 1,}).exec((err, admins)=>{
        res.render('modules/admin/admin',{admins : admins});
    })
})

app.get('/adminDetails',(req,res)=>{
    console.log('adminDetails : /')
    
    if(req.query.adminId){
        let adminId = req.query.adminId
        console.log('adminDetails : /' + adminId)
        Admin.find({_id : adminId}).exec((err, admin)=>{
            // console.log(news)
            res.render('modules/admin/admin_details',{admin : admin[0]});
        })
    }else{
        console.log('newsDetailsNew : /')
        res.render('modules/admin/admin_details',{admin : ""});
    }
})

app.get('/user',(req,res)=>{
    console.log('user : /')
    User.find().exec((err, users)=>{
        // console.log(users)
        res.render('modules/user/user',{users : users});
    })
})

app.get('/userDetails',(req,res)=>{
    if(req.query.userId){
        let userId = req.query.userId
        console.log('userDetails : /' + userId)
        User.find({userId : userId}).exec((err, users)=>{
            // console.log(news)
            res.render('modules/user/user_details',{users : users[0]});
        })
    }else{
        console.log('userDetailsNew : /')
        res.render('modules/user/user_details',{users : ""});
    }
})

app.get('/pointHistory',(req,res)=>{
    console.log('pointHistory : /')
        res.render('modules/pointHistory/pointHistory',{news : ""});
})

app.get('/voucher',(req,res)=>{
    console.log('voucher : /')
    // News.find().exec((err, news)=>{
        // console.log(news)
        res.render('modules/voucher/voucher',{news : ""});
    // })
})

module.exports = app;