let express = require('express');
let router = express.Router();
let multiparty = require('connect-multiparty');
let async = require('async');
let request = require('request');
let http = require('http');
let moment = require('moment');
let fs = require('fs');
// let nodemailer = require('nodemailer');
var randomize = require('randomatic');

let Voucher = require('../../models/voucher/voucher');
let User = require('../../models/user/user');
let fbAccountKit = require('../../models/fbAccountKit/index');
let MemberId = require('../../models/memberId/index');
let News = require('../../models/news/news');

let xilnexHeaders = {
    auth : "5",
    token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
    appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
}

var multipartyMiddleware = multiparty();
router.use(multipartyMiddleware);
// let emailConfig = JSON.parse(fs.readFileSync('./config/emailConfig.json'));
// let transporter = nodemailer.createTransport({emailConfig});
// console.log(emailConfig.auth.user);

// router.get('/newfbAccountKiterId',function(req,res,next){
//     let newfbAccountKiterId = new fbAccountKit({
//         authorizationAccessToken : "AA|891765251170383|3e1c7589005d5114b7cc209dfb7549ed",
//     })
//     newfbAccountKiterId.save((err, done) =>{
//         res.json({status : 200})
//     })
// })

router.post('/user-registration', function(req, res, next) {
    console.log('POST - /user-registration')
    let phoneNumber = req.body.phoneNumber
    let randomCode = /* randomize('0', 6) */ 201044 

    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/query?mobile=' + phoneNumber,
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        let clientDetails = JSON.parse(body)
        if (!error && response.statusCode == 200 && clientDetails.data.clients.length > 0) {
        //   res.json(clientDetails.data.clients);process.exit();
            if(clientDetails.data.clients[0].customFieldValue1 == "" || clientDetails.data.clients[0].customFieldValue1 == "register"){
                let client = clientDetails.data.clients[0]
                client.customFieldValue1 = "register"
                client.customFieldValue2 = randomCode

                var option2 = {
                    method: 'PUT',
                    url: 'https://api.xilnex.com/logic/v2/clients/' + client.id,
                    body: {client : client},
                    json: true,
                    headers: {
                        auth : "5",
                        token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
                        appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
                    }
                }
                // res.json(option2)
                request(option2, (err2, res2, body2) => {
                    if(body2.status == "SuccessUpdate"){
                        res.json({
                            status : 200,
                            msg : "",
                            verificationCode : randomCode 
                        })
                    }else{
                        res.json({
                            status : 300,
                            msg : "User already existed"
                        })
                    }
                });

            }else{
                res.json({
                    status : 300,
                    msg : "User already existed"
                })
                
            }
        } else{
            let client = {
                "mobile" : phoneNumber,
                "customFieldValue1" : "register",
                "customFieldValue2" : randomCode,
                "active": true,
                "createdOutlet": "1U",
                "name": "Your Name"
            }

            let option2 = {
                method: 'POST',
                url: 'https://api.xilnex.com/logic/v2/clients/client',
                body: {client : client},
                json: true,
                headers: {
                    auth : "5",
                    token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
                    appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
                }
            }
            // res.json(option2)
            request(option2, (err2, res2, body2) => {
                // JSON.parse(body2)
                // console.log(body2)
                if(body2.status == "SuccessInsert"){
                    res.json({
                        status : 200,
                        msg : "",
                        verificationCode : randomCode 
                    })
                }else{
                    res.json({
                        status : 300,
                        msg : "User already existed"
                    })
                }
            })
        }        
    })
})

router.post('/user-verification', function(req,res,next){
    console.log('POST - /user-verification')
    let phoneNumber = req.body.phoneNumber
    let verificationCode = req.body.verificationCode
    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/query?mobile=' + phoneNumber,
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        if (!error && response.statusCode == 200) {
            let clientDetails = JSON.parse(body)
            if(clientDetails.data.clients[0].customFieldValue1 == "register" || clientDetails.data.clients[0].customFieldValue2 == verificationCode){
                let client = clientDetails.data.clients[0]
                res.json({
                    status : 200,
                    userId : client.id,
                    firstName : client.firstName,
                    lastName : client.lastName,
                    ic : client.ic,
                    dob : client.dob,
                    gender : client.gender,

                    maritalStatus : client.customFieldValue3,

                    race : client.race,

                    street : client.shipping != null && client.shipping.length > 0 ? client.shipping.street : "",
                    city : client.shipping != null && client.shipping.length > 0 ? client.shipping.city : "",
                    state : client.shipping != null && client.shipping.length > 0 ? client.shipping.state : "",
                    zipcode : client.shipping != null && client.shipping.length > 0 ? client.shipping.zipcode : "",
                    country : client.shipping != null && client.shipping.length > 0 ? client.shipping.country : "",

                    phoneNumber : client.mobile,
                    email : client.email,

                    occupation : client.customFieldValue4,

                    msg : ""	
                })
            }else{
                res.json({
                    status : 400,
                    msg : "wrong verification code"
                })
            }
        
        }else {
            res.json({
                status : 400,
                msg : "Invalid Data"
            })
        }
    })


})

router.post('/new-user', function(req, res, next) {
    console.log('POST - /new-user')
    let data = {
        userId : req.body.userId,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        name : req.body.firstName + " " + req.body.lastName,
        ic : req.body.ic,
        dob : req.body.dob ? req.body.dob : "",
        gender : req.body.gender ? req.body.gender : "",

        maritalStatus : req.body.maritalStatus ? req.body.maritalStatus : "",

        race : req.body.race ? req.body.race : "",
        street : req.body.street ? req.body.street : "",
        city : req.body.city ? req.body.city : "",
        state : req.body.state ? req.body.state : "",
        zipcode : req.body.zipcode ? req.body.zipcode : "",
        country : req.body.country ? req.body.country : "",

        address : req.body.street + " " + req.body.city + " " + req.body.zipcode + " " + req.body.state + " " + req.body.country,

        phoneNumber : req.body.phoneNumber,
        email : req.body.email,

        occupation : req.body.occupation ? req.body.occupation : "",

        password : req.body.password
    }
    //   res.json(data);process.exit();

    
    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/query?mobile=' + data.phoneNumber,
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        let clientDetails = JSON.parse(body)
        // res.json(clientDetails)
        if(clientDetails.data.clients[0].customFieldValue1 == "register"){
            let client = clientDetails.data.clients[0]
            client.firstName = data.firstName
            client.lastName = data.lastName
            client.name = data.name
            client.ic = data.ic
            client.registrationCode = data.ic
            client.dob = data.dob
            client.gender = data.gender

            client.customFieldValue3 = data.maritalStatus,

            client.race = data.race

            client.shippingAddress = data.address
            client.shipping = {
                street : data.street,
                city : data.city,
                state : data.state,
                zipcode : data.zipcode,
                country : data.country
            },

            client.mobile = data.phoneNumber
            client.email = data.email

            client.customFieldValue4 = data.occupation,

            client.customFieldValue1 = "active"
            // console.log(data.email)
            // res.json(client)
            
            var option2 = {
                method: 'PUT',
                url: 'https://api.xilnex.com/logic/v2/clients/' + client.id,
                body: {client : client},
                json: true,
                headers: {
                    auth : "5",
                    token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
                    appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
                }
            }
            // res.json(option2)
            request(option2, (err2, res2, body2) => {
                if(body2.status == "SuccessUpdate"){
                let newUser = new User({
                    userId : client.id,
                    phoneNumber : data.phoneNumber,
                    firstName : data.firstName,  
                    lastName : data.lastName,  
                    email : data.email,
                    ic : data.ic,
                    dob : data.dob,
                    gender : data.gender,
                    maritalStatus : data.maritalStatus,
                    race : data.race,

                    address : data.address,
                    
                    street : data.street,
                    city : data.city,
                    state : data.state,
                    zipcode : data.zipcode,
                    country : data.country,

                    occupation : data.occupation,

                    status : "active",

                    createdAt : moment().unix(),
                    LastLogin : moment().unix(),

                    password : data.password
                })
                newUser.save((err, done) =>{
                    if(done){
                        res.json({
                            status : 200,
                            userId : client.id,
                            firstName : client.firstName,
                            lastName : client.lastName,
                            ic : client.ic,
                            dob : client.dob,
                            gender : client.gender,

                            maritalStatus : data.maritalStatus,

                            race : client.race,
                            
                            // address : data.address,
                            
                            street : data.street,
                            city : data.city,
                            state : data.state,
                            zipcode : data.zipcode,
                            country : data.country,

                            phoneNumber : client.mobile,
                            email : data.email,

                            occupation : data.occupation,
                            msg : ""	
                        })
                    }else{
                        res.json({
                            status : 400,
                            msg : "Sorry! Error occur"
                        })
                    }
                })
                }else{
                    // console.log(body)
                    res.json({
                        status : 300,
                        msg : "User already existed"
                    })
                }
            });
            

        }else{
            res.json({
                status : 300,
                msg : "User already existed"
            })
        }
    })
})

router.get('/userDetails/:userId', function(req, res, next) {
    let userId = req.params.userId
    console.log('GET - /userDetails/' + userId)
    User.find({userId : userId}).exec((err, user)=>{
        if(user.length > 0){
            let userDetails = user[0]
            res.json({
                status : 200,
                userId : userDetails.userId,
                firstName : userDetails.firstName,
                lastName : userDetails.lastName,
                ic : userDetails.ic,
                dob : userDetails.dob,
                gender : userDetails.gender,

                maritalStatus : userDetails.maritalStatus,

                race : userDetails.race,
                // address : userDetails.address,
                
                street : userDetails.street,
                city : userDetails.city,
                state : userDetails.state,
                zipcode : userDetails.zipcode,
                country : userDetails.country,

                phoneNumber : userDetails.phoneNumber,
                email : userDetails.email,

                occupation : userDetails.occupation,

                msg : ""	
            })
        }else{
            res.json({
                status : 400,
                msg : "User not found!"
            })
        }
    })
})

router.get('/userPoint/:userId', function(req,res,next){
    let userId = req.params.userId
    console.log('GET - /userPoint/' + userId)
    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/' + userId,
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        if(error){
            res.json({
                status : 400,
                msg : "User not found!"
            })
        }else{
            let clientDetails = JSON.parse(body)
            // console.log(clientDetails.data.client.pointValue)
            res.json({
                status : 200,
                point : clientDetails.data.client.pointValue,
                msg : ""
            })
        }
    })

})

router.get('/userPointHistory/:userId', function(req,res,next){
    let userId = req.params.userId
    console.log('GET - /userPointHistory/' + userId)
    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/' + userId + '/pointrecords?datefrom=2015-01-01T00:00:00.000Z&dateto=' + moment().toISOString(),
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        if(error){
            res.json({
                status : 400,
                msg : "User not found!"
            })
        }else{
            let pointHistory = JSON.parse(body)
            // console.log(pointHistory.data.pointRecords)
            let historyList = []
            async.each(pointHistory.data.pointRecords,(eachHistory,callback)=>{
                historyList.push({
                    recordDate : eachHistory.recordDate,
                    outlet : eachHistory.outlet,
                    pointAdjustment : eachHistory.pointAdjustment,
                    remark : eachHistory.remark
                })
                callback()
            },(err)=>{
                res.json({
                    status : 200,
                    historyList : historyList,
                    msg : ""
                })
            })
        }
    })

})

router.post('/login', function(req, res, next) {
    console.log('POST - /login')
    let phoneNumber = req.body.phoneNumber
    let password = req.body.password

    User.find({phoneNumber : phoneNumber}).exec((err, user)=>{
        // console.log(playerDetails)
        if(user.length > 0){
            let userDetails = user[0];
            if(userDetails.password == password){
                res.json({
                    status : 200,
                    userId : userDetails.userId,
                    firstName : userDetails.firstName,
                    lastName : userDetails.lastName,
                    ic : userDetails.ic,
                    dob : userDetails.dob,
                    gender : userDetails.gender,
    
                    maritalStatus : userDetails.maritalStatus,
    
                    race : userDetails.race,
                    // address : userDetails.address,
                    
                    street : userDetails.street,
                    city : userDetails.city,
                    state : userDetails.state,
                    zipcode : userDetails.zipcode,
                    country : userDetails.country,
    
                    phoneNumber : userDetails.phoneNumber,
                    email : userDetails.email,
    
                    occupation : userDetails.occupation,
                    msg : ""
                })
            }else{
                res.json({
                    status : 400,
                    msg : "Invalid password!"
                })
            }
        }else{
            res.json({
                status : 400,
                msg : "User not found!"
            })
        }

    })
})

router.post('/resetPassword', function(req, res, next) {
    console.log('POST - /resetPassword')
    let phoneNumber = req.body.phoneNumber
    let randomCode = /* randomize('0', 6) */ 201044

    User.findOneAndUpdate({phoneNumber: phoneNumber}, {password : randomCode}).exec(function(err, done) {
        if(err){
            console.log(err)
            res.json({
                status : 400,
                msg : "Reset password failed!"
            })
        }else{
            res.json({
                status : 200,
                password : randomCode,
                msg : "Reset password success. Please re-login"
            })
            console.log("new password : " + randomCode)
        }
    })
})

router.post('/updatePassword', function(req, res, next) {
    console.log('POST - /updatePassword')
    let phoneNumber = req.body.phoneNumber
    let newPassword = req.body.newPassword
    let oldPassword = req.body.oldPassword

    User.findOneAndUpdate({phoneNumber: phoneNumber, password : oldPassword}, {password : newPassword}).exec(function(err, done) {
        if(err){
            console.log(err)
            res.json({
                status : 400,
                msg : "Update password failed!"
            })
        }else{
            res.json({
                status : 200,
                msg : "Update password success."
            })

        }
    })
})

router.post('/updateProfile', function(req,res,next){
    console.log('POST - /updateProfile')
    let data = {
        userId : req.body.userId,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        name : req.body.firstName + " " + req.body.lastName,
        ic : req.body.ic,
        dob : req.body.dob,
        gender : req.body.gender,
        maritalStatus : req.body.maritalStatus,
        race : req.body.race,
        address : req.body.street + " " + req.body.city + " " + req.body.zipcode + " " + req.body.state + " " + req.body.country,
        street : req.body.street,
        city : req.body.city,
        state : req.body.state,
        zipcode : req.body.zipcode,
        country : req.body.country,
        phoneNumber : req.body.phoneNumber,
        email : req.body.email,
        occupation : req.body.occupation,
    }

    
    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/' + data.userId,
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        if(error){
            res.json({
                status : 400,
                msg : "User not found!"
            })
        }else{
            let userDetails = JSON.parse(body).data != null ? JSON.parse(body).data.client : ""
            // console.log(userDetails)

            if (userDetails != ""){
                // res.json({status:200})
    
                    // let client = clientDetails.data.clients[0]
                    userDetails.firstName = data.firstName
                    userDetails.lastName = data.lastName
                    userDetails.name = data.name
                    userDetails.ic = data.ic
                    userDetails.registrationCode = data.ic
                    userDetails.dob = data.dob
                    userDetails.gender = data.gender
                    userDetails.customFieldValue3 = data.maritalStatus
                    userDetails.race = data.race
                    userDetails.shippingAddress = data.address
                    userDetails.shipping = {
                        street : data.street,
                        city : data.city,
                        state : data.state,
                        zipcode : data.zipcode,
                        country : data.country
                    }
                    userDetails.mobile = data.phoneNumber
                    userDetails.email = data.email
                    userDetails.customFieldValue4 = data.occupation
                    
                    // console.log(userDetails)
                    var option2 = {
                        method: 'PUT',
                        url: 'https://api.xilnex.com/logic/v2/clients/' + userDetails.id,
                        body: {client : userDetails},
                        json: true,
                        headers: {
                            auth : "5",
                            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
                            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
                        }
                    }
                    // res.json(option2)
                    request(option2, (err2, res2, body2) => {
                        if(body2.status == "SuccessUpdate"){
                            User.findOneAndUpdate({userId : data.userId}, {
                                userId : data.userId,
                                phoneNumber : data.phoneNumber,
                                firstName : data.firstName,  
                                lastName : data.lastName,  
                                email : data.email,
                                ic : data.ic,
                                dob : data.dob,
                                gender : data.gender,
                                maritalStatus : data.maritalStatus,
                                race : data.race,
                                address : data.address,                                
                                street : data.street,
                                city : data.city,
                                state : data.state,
                                zipcode : data.zipcode,
                                country : data.country,
                                occupation : data.occupation,
                                LastLogin : moment().unix()
                            }).exec(function(err, success) {
                                if(success){
                                    res.json({
                                        status : 200,
                                        msg : "Update profile success."
                                    })                                  
                                }else{
                                    res.json({
                                        status : 400,
                                        msg : "Update profile failed."
                                    })   
                                }
                            })
                        }else{
                            res.json({
                                status : 400,
                                msg : "Update profile failed."
                            })   
                        }
                    })

            }else{
                res.json({
                    status : 400,
                    msg : "User not found!"
                })
            }
        }
    })

})

router.post('/addNews', function(req,res,next){
    console.log('POST - /addNews')
    let newNews = new News({
        title : "XPLORE X7 Satellite Phone Promotion",
        subtitle : "XPLORE X7 Satellite Phone Promotion",
        content : `<p><strong>Bucked Out Seafood Reunion</strong></p>

        <p>4, 5 &amp; 6 February 2019</p>
        
        <p>6.30pm to 10.30pm</p>
        
        <p>RM168 nett&nbsp;per person<br />
        <br />
        &nbsp;</p>
        
        <p><strong>Bucked Out Seafood</strong></p>
        
        <p>Every Friday &amp; Saturday</p>
        
        <p>6.30pm to 10.30pm</p>
        
        <p>Rm149 nett per person</p>
        
        <p>&nbsp;</p>
        
        <p>For reservation or enquiries, please call (+603-23329818) or email gobochitchat.thkl@tradershotels.com.</p>
        `,
        slider : 'active',
        status : 'active',
        publishedAt : '2018-07-11 11:27:51',
        expiredAt : '2020-07-11 11:27:51',
        imageSrc : "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/300px-No_image_available.svg.png",
        sortOrder : '1',
    })
    newNews.save((err, done) =>{
        res.json({status : 200})
    })
})

router.get('/getSliderNews', function(req,res,next){
    console.log('GET - /getSliderNews')
    News.find({slider : 'active', status:'active'},{/* title:1,subtitle:1, */imageSrc:1,sortOrder:1}).exec((err, slider)=>{
        // res.json(slider);
        if(slider.length > 0){
            let sliders = []
            async.each(slider,(eachSlider,callback)=>{
                sliders.push({
                    sliderId : eachSlider._id,
                    // title : eachSlider.title,
                    // subtitle : eachSlider.subtitle,
                    imageSrc : eachSlider.imageSrc,
                    sortOrder : eachSlider.sortOrder
                })
                callback()
            },(err)=>{
                res.json({
                    status : 200,
                    sliders : sliders,
                    msg : ""
                })
            })
        }else{
            res.json({
                status : 400,
                sliders : [],
                msg : "No slider found!"
            })
        }
    })
})

router.get('/getNews', function(req,res,next){
    console.log('GET - /getNews')
    News.find({status:'active'},{title:1,subtitle:1,imageSrc:1,sortOrder:1}).exec((err, news)=>{
        // res.json(slider);
        if(news.length > 0){
            let newsAll = []
            async.each(news,(eachNews,callback)=>{
                newsAll.push({
                    newsId : eachNews._id,
                    title : eachNews.title,
                    subtitle : eachNews.subtitle,
                    imageSrc : eachNews.imageSrc,
                    sortOrder : eachNews.sortOrder
                })
                callback()
            },(err)=>{
                res.json({
                    status : 200,
                    news : newsAll,
                    msg : ""
                })
            })
        }else{
            res.json({
                status : 400,
                news : [],
                msg : "No news found!"
            })
        }
    })
})

router.get('/getNewsDetails/:newsId', function(req,res,next){
    let newsId = req.params.newsId
    console.log('GET - /getNewsDetails/' + newsId)
    
    News.findById(newsId).exec((err, newsDetails)=>{
        if(newsDetails){
            res.json({
                status : 200,
                newsId : newsDetails._id,
                title : newsDetails.title,
                subtitle : newsDetails.subtitle,
                imageSrc : newsDetails.imageSrc,
                content : newsDetails.content,
                msg : ""
            })
        }else{
            res.json({
                status : 400,
                msg : "News not found!"
            })
        }
    })

})

module.exports = router;