const express = require('express');

let app = express();

app.get('/', isLoggedIn, (req,res)=>{
    console.log('dashboard : /')
    res.render('modules/dashboard/dashboard',{ admin : req.session.admin});
    // res.send()
})

module.exports = app;


function isLoggedIn (req, res, next) {
	if (req.session.admin) {
		return next();
	}
	res.redirect('/login');
}