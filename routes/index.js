const express = require('express');
const bcrypt = require('bcrypt');
const moment = require('moment');
let async = require('async');
let request = require('request');
let path = require('path');

let app = express();
let News = require('../models/news/news');
let User = require('../models/user/user');
let Admin = require('../models/admin/admin');
let Voucher = require('../models/voucher/voucher');
let fbAccountKit = require('../models/fbAccountKit/index');
let MemberId = require('../models/memberId/index');
// let PointHistory = require('../models/pointHistory/pointHistory');

const upload = require('../uploadMiddleware');
const Resize = require('../Resize');

let xilnexHeaders = {
    auth : "5",
    token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
    appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
}

app.get('/', isLoggedIn,(req,res)=>{
    res.redirect('/dashboard');
})

// app.post('/dashboard',(req,res)=>{
//     console.log('dashboard : /')
//     // News.find().exec((err, news)=>{
//         // console.log(news)
//         res.render('modules/dashboard/dashboard',{news : ""});
//     // })
// })

app.get('/login',(req,res)=>{
    console.log('login : /')
    // News.find().exec((err, news)=>{
        // console.log(news)
        res.render('modules/login/index',{admin : req.session.admin});
    // })
})

/* ---------------------------------------------------------news------------------------------------------------------------- */

app.get('/news', isLoggedIn, (req,res)=>{
    console.log('news : /')
    News.find().exec((err, news)=>{
        // console.log(news)
        res.render('modules/news/news',{news : news, admin : req.session.admin});
    })
})

// app.post('/updateNews',(req,res)=>{
//     console.log('news : /')
//     News.find().exec((err, news)=>{
//         // console.log(news)
//         res.render('modules/news/news',{news : news, admin : req.session.admin});
//     })
// })

app.get('/newsDetails', isLoggedIn, (req,res)=>{
    if(req.query.newsId){
        let newsId = req.query.newsId
        console.log('newsDetails : /' + newsId)
        News.find({_id : newsId}).exec((err, news)=>{
            // console.log(news)
            res.render('modules/news/news_details',
                {news : {
                    _id : news[0]._id,
                    title : news[0].title,
                    subtitle : news[0].subtitle,
                    content : news[0].content,
                    slider : news[0].slider,
                    status : news[0].status,
                    publishedAt : moment(news[0].publishedAt).format('YYYY-MM-DDTHH:mm'),
                    expiredAt : moment(news[0].expiredAt).format('YYYY-MM-DDTHH:mm'),
                    imageSrc : news[0].imageSrc,
                    sortOrder : news[0].sortOrder
                }, admin : req.session.admin}
            );
        })
    }else{
        console.log('newsDetailsNew : /')
        res.render('modules/news/news_details',{ admin : req.session.admin});
    }
})

app.get('/addNews', isLoggedIn, (req,res)=>{
    res.render('modules/news/addNews',{ admin : req.session.admin});
})

app.post('/addNews', upload.single('image'), isLoggedIn, async (req,res)=>{
    const imagePath = path.join(__dirname, '../assets/img/news');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
      res.status(401).json({error: 'Please provide an image'});
    }else{
        const filename = await fileUpload.save(req.file.buffer);

        let newNews = new News({
            title : req.body.title,
            subtitle : req.body.subtitle,
            content : req.body.content,
            slider : req.body.slider,
            status : req.body.status,
            sortOrder : req.body.sortOrder,
            imageSrc : filename ? "http://" + req.headers.host + "/img/news/" + filename : "https://icon-library.net/images/news-icon-png/news-icon-png-18.jpg",
            publishedAt : moment().format('YYYY-MM-DD HH:mm:ss'),
            expiredAt : moment(req.body.expiredAt).format('YYYY-MM-DD HH:mm:ss')
        })
    
        // console.log(newNews)
        newNews.save((err, done) =>{
            if(done){
                res.redirect('/news');
            }else{
                console.log(err)
            }
        })
    }
})

app.post('/updateNewsDetails',upload.single('image'), isLoggedIn, async (req,res)=>{
    console.log('updateNewsDetails : /' + req.body.id)
    let update = {}
    req.body.title ? update.title = req.body.title : ""
    req.body.subtitle ? update.subtitle = req.body.subtitle : ""
    req.body.content ? update.content = req.body.content : ""
    req.body.slider ? update.slider = req.body.slider : ""
    req.body.status ? update.status = req.body.status : ""
    req.body.sortOrder ? update.sortOrder = req.body.sortOrder : ""
    // req.file ? update.imageSrc = "http://" + req.headers.host + "/img/news/" + imageSrc : ""
    req.body.publishedAt ? update.publishedAt = moment(req.body.publishedAt).format('YYYY-MM-DD HH:mm:ss') : ""
    req.body.expiredAt ? update.expiredAt = moment(req.body.expiredAt).format('YYYY-MM-DD HH:mm:ss') : ""

    if (!req.file) {
    //   res.status(401).json({error: 'Please provide an image'});

        News.findOneAndUpdate({_id : req.body.id},update).exec((err,done)=>{
            if(done){
                res.redirect('/news');
            }else{
                console.log(err)
            }
        })
    }else{
        const imagePath = path.join(__dirname, '../assets/img/news');
        const fileUpload = new Resize(imagePath);
        const imageSrc = await fileUpload.save(req.file.buffer);

        req.file ? update.imageSrc = "http://" + req.headers.host + "/img/news/" + imageSrc : ""

        News.findOneAndUpdate({_id : req.body.id},update).exec((err,done)=>{
            if(done){
                res.redirect('/news');
            }else{
                console.log(err)
            }
        })
    }
})

app.get('/deleteNews/:newsId', isLoggedIn, (req,res)=>{
    let newsId = req.params.newsId
    console.log('deleteNews : /' + newsId)
    News.remove({_id : newsId}).exec((err, done)=>{
        if(err){
            console.log(err)
        }
        res.redirect('/news');
    })    
})

/* ---------------------------------------------------------admin------------------------------------------------------------- */

app.post('/loginAdmin', function(req,res,next){
    let name = req.body.username
    let password = req.body.password

    Admin.findOne({name : name}).exec((err, admin)=>{
        if(bcrypt.compareSync(password, admin.password)){
            req.session.admin = admin;
            // console.log(req.session.admin)
            res.redirect('/dashboard');
        }else{
            res.json({status : 400})
        }
    })
})

app.get('/admin', isLoggedIn, (req,res)=>{
    console.log('admin : /')
    Admin.find({},{name : 1, email : 1, level : 1, status : 1, createdAt : 1,}).exec((err, admins)=>{
        res.render('modules/admin/admin',{admins : admins, admin : req.session.admin});
    })
})

app.get('/adminDetails', isLoggedIn, (req,res)=>{
    console.log('adminDetails : /')
    
    if(req.query.adminId){
        let adminId = req.query.adminId
        console.log('adminDetails : /' + adminId)
        Admin.find({_id : adminId}).exec((err, admin)=>{
            console.log("here : "+moment(parseInt(admin[0].createdAt*1000)).format('YYYY-MM-DDTHH:mm'))
            res.render('modules/admin/admin_details',{adminDetails : {
                _id : admin[0]._id,
                name : admin[0].name,
                email : admin[0].email,
                level : admin[0].level,
                status : admin[0].status,
                createdAt : moment(parseInt(admin[0].createdAt*1000)).format('YYYY-MM-DDTHH:mm'),
            },  admin : req.session.admin});
        })
    }else{
        console.log('newsDetailsNew : /')
        res.render('modules/admin/admin_details',
            {admin : req.session.admin});
    }
})

app.post('/updateAdminDetails', isLoggedIn, (req,res)=>{
    console.log(req.body._id)
    let query = {}
    req.body.name ? query.name = req.body.name : ""
    req.body.email ? query.email = req.body.email : ""
    req.body.level ? query.level = req.body.level : ""
    req.body.status ? query.status = req.body.status : ""
    req.body.createdAt ? query.createdAt = moment(req.body.createdAt).unix() * 1000: ""
    // console.log(query)
    Admin.findOneAndUpdate({_id : req.body._id},query).exec((err, done)=>{
        if(done){
            res.redirect('/admin');
        }else{
            console.log(err)
        }        
    })
})

app.get('/addAdmin', isLoggedIn, (req,res)=>{
    res.render('modules/admin/addAdmin',{ admin : req.session.admin});
})

app.post('/addAdmin', isLoggedIn, (req,res)=>{
    // res.json(req.body)
    let plainPassword = req.body.password 
    let saltRounds = 10;
    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(plainPassword, salt);

    let newAdmin = new Admin({
        name : req.body.username,
        email :req.body.email,
        level : req.body.level,
        status : req.body.status,
        createdAt : moment().unix(),
        password : hash,
    })

    newAdmin.save((err, done) =>{
        res.redirect('/admin');
    })   
})

/* ---------------------------------------------------------user------------------------------------------------------------- */

app.get('/user', isLoggedIn, (req,res)=>{
    console.log('user : /')
    User.find().exec((err, users)=>{
        // console.log(users)
        res.render('modules/user/user',{users : users, admin : req.session.admin});
    })
})

app.get('/userDetails', isLoggedIn,(req,res)=>{
    if(req.query.userId){
        let userId = req.query.userId
        console.log('userDetails : /' + userId)
        User.find({userId : userId}).exec((err, users)=>{
            // console.log(news)
            res.render('modules/user/user_details',{users : {
                _id : users[0]._id, 
                userId : users[0].userId,
                phoneNumber : users[0].phoneNumber, 
                status : users[0].status, 
                firstName : users[0].firstName, 
                lastName : users[0].lastName, 
                ic : users[0].ic, 
                dob : moment(users[0].dob).format('YYYY-MM-DD'),
                gender : users[0].gender, 
                maritalStatus : users[0].maritalStatus, 
                race : users[0].race, 
                street : users[0].street, 
                city : users[0].city, 
                state : users[0].state, 
                zipcode : users[0].zipcode, 
                country : users[0].country, 
                email : users[0].email, 
                occupation : users[0].occupation, 
                alternateLookup : users[0].alternateLookup, 
                code : users[0].code, 
            }, admin : req.session.admin});
        })
    }else{
        console.log('userDetailsNew : /')
        res.render('modules/user/user_details',{users : ""});
    }
})

app.post('/updateUserDetails', isLoggedIn, (req,res)=>{
    console.log('updateUserDetails : /')
    let data = {
        userId : req.body.userId ? req.body.userId : "",
        firstName : req.body.firstName ? req.body.firstName : "",
        lastName : req.body.lastName ? req.body.lastName : "",
        name : req.body.firstName ? req.body.firstName : "" + " " + req.body.lastName ? req.body.lastName : "",
        ic : req.body.ic ? req.body.ic : "",
        // dob : req.body.dob ? req.body.dob : "",
        gender : req.body.gender ? req.body.gender : "",
        maritalStatus : req.body.maritalStatus ? req.body.maritalStatus : "",
        race : req.body.race ? req.body.race : "",
        address : (req.body.street ? req.body.street : "") + " " + (req.body.city ? req.body.city : "") + " " + (req.body.zipcode ? req.body.zipcode : "") + " " + (req.body.state ? req.body.state : "") + " " + (req.body.country ? req.body.country : ""),
        street : req.body.street ? req.body.street : "",
        city : req.body.city ? req.body.city : "",
        state : req.body.state ? req.body.state : "",
        zipcode : req.body.zipcode ? req.body.zipcode : "",
        country : req.body.country ? req.body.country : "",
        phoneNumber : req.body.phoneNumber ? req.body.phoneNumber : "",
        email : req.body.email ? req.body.email : "",
        occupation : req.body.occupation ? req.body.occupation : "",
    }
    // res.json(data)

    
    var options = {
        url: 'https://api.xilnex.com/logic/v2/clients/' + data.userId,
        headers: {
            auth : "5",
            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
        },
        method: 'GET',
    }
    request.get(options, function(error,response,body) {
        if(error){
            // res.json({
            //     status : 400,
            //     msg : "User not found!"
            // })
            console.log(error)
            res.redirect('/user');
        }else{
            let userDetails = JSON.parse(body).data != null ? JSON.parse(body).data.client : ""
            // console.log(userDetails)

            if (userDetails != ""){
                // res.json({status:200})
    
                    // let client = clientDetails.data.clients[0]
                    userDetails.firstName = data.firstName
                    userDetails.lastName = data.lastName
                    userDetails.name = data.name
                    userDetails.ic = data.ic
                    userDetails.registrationCode = data.ic
                    // userDetails.dob = data.dob
                    userDetails.gender = data.gender
                    userDetails.customFieldValue3 = data.maritalStatus
                    userDetails.race = data.race
                    userDetails.shippingAddress = data.address
                    userDetails.shipping = {
                        street : data.street,
                        city : data.city,
                        state : data.state,
                        zipcode : data.zipcode,
                        country : data.country
                    }
                    userDetails.mobile = data.phoneNumber
                    userDetails.email = data.email
                    userDetails.customFieldValue4 = data.occupation
                    
                    // console.log(userDetails)
                    var option2 = {
                        method: 'PUT',
                        url: 'https://api.xilnex.com/logic/v2/clients/' + userDetails.id,
                        body: {client : userDetails},
                        json: true,
                        headers: {
                            auth : "5",
                            token : "zhGC3GC4OwzE1d4xvwpfRlfO9KqOAqBhXnuZWpeSNjM=",
                            appid : "NW5bmyRShYNPiobQ6qxOo1ARyGM43gxM"
                        }
                    }
                    // res.json(option2)
                    request(option2, (err2, res2, body2) => {
                        if(body2.status == "SuccessUpdate"){
                            User.findOneAndUpdate({userId : data.userId}, {
                                userId : data.userId,
                                phoneNumber : data.phoneNumber,
                                firstName : data.firstName,  
                                lastName : data.lastName,  
                                email : data.email,
                                ic : data.ic,
                                // dob : data.dob,
                                gender : data.gender,
                                maritalStatus : data.maritalStatus,
                                race : data.race,
                                address : data.address,                                
                                street : data.street,
                                city : data.city,
                                state : data.state,
                                zipcode : data.zipcode,
                                country : data.country,
                                occupation : data.occupation,
                                LastLogin : moment().unix()
                            }).exec(function(err, success) {
                                if(success){
                                    // res.json({
                                    //     status : 200,
                                    //     msg : "Update profile success."
                                    // })
                                    // console.log(error)
                                    res.redirect('/user');                                  
                                }else{
                                    // res.json({
                                    //     status : 400,
                                    //     msg : "Update profile failed."
                                    // })  
                                    console.log(err)
                                    res.redirect('/user'); 
                                }
                            })
                        }else{
                            // res.json({
                            //     status : 400,
                            //     msg : "Update profile failed."
                            // })   
                            console.log(err2)
                            res.redirect('/user');
                        }
                    })

            }else{
                // res.json({
                //     status : 400,
                //     msg : "User not found!"
                // })
                console.log("userDetails == ")
                res.redirect('/user');
            }
        }
    })
})

/* ---------------------------------------------------------point history------------------------------------------------------------- */

app.get('/pointHistory', isLoggedIn,(req,res)=>{
    if(req.query.userId){
        let userId = req.query.userId
        console.log('pointHistory : /' + userId)
        var options = {
            url: 'https://api.xilnex.com/logic/v2/clients/' + userId + '/pointrecords?datefrom=2015-01-01T00:00:00.000Z&dateto=' + moment().toISOString(),
            headers: xilnexHeaders,
            method: 'GET',
        }
        request.get(options, function(error,response,body) {
            if(error){
                res.render('modules/pointHistory/pointHistory',{pointHistory : [], admin : req.session.admin});
            }else{
                let pointHistory = JSON.parse(body)
                // console.log(pointHistory.data.pointRecords)
                let historyList = []
                async.each(pointHistory.data.pointRecords,(eachHistory,callback)=>{
                    historyList.push({
                        recordDate : eachHistory.recordDate,
                        outlet : eachHistory.outlet,
                        pointAdjustment : eachHistory.pointAdjustment,
                        remark : eachHistory.remark
                    })
                    callback()
                },(err)=>{
                    // console.log(historyList)
                    res.render('modules/pointHistory/pointHistory',{pointHistory : historyList, admin : req.session.admin});
                })
            }
        })

    }else{
        console.log('pointHistory1 : /')
        res.render('modules/pointHistory/pointHistory',{pointHistory : [], admin : req.session.admin});
    }
})

/* ---------------------------------------------------------voucher------------------------------------------------------------- */

app.get('/addVoucher', isLoggedIn,(req,res)=>{
    res.render('modules/voucher/addVoucher',{admin : req.session.admin});
})

app.post('/addVoucher', upload.single('image'), isLoggedIn, async (req,res)=>{
    console.log('addVoucher : /')    
    const imagePath = path.join(__dirname, '../assets/img/voucher');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
      res.status(401).json({error: 'Please provide an image'});
    }else{
        const filename = await fileUpload.save(req.file.buffer);
        let codes = req.body.codes.split(",")
        async.each(codes,(eachCodes,callback)=>{
            if(eachCodes.split(' ').join('') != ""){
                var newVoucher = new Voucher ({
                    code : eachCodes.split(' ').join(''),
                    title : req.body.title,
                    subtitle : req.body.subtitle,
                    content : req.body.content,
                    cost : req.body.cost,
                    status : req.body.status,
                    createdAt : moment().unix(),
                    expiredAt : req.body.expiredAt,
                    imageSrc : filename ? "http://" + req.headers.host + "/img/voucher/" + filename : "",
                    sortOrder : req.body.sortOrder,
                })
                newVoucher.save(function(err,done){
                    if (err) {console.log(err)}
                    callback()
                })
            }else{
                callback();
            }
        
        },(err)=>{
            res.redirect('/voucher');
        })
    }
})

app.get('/voucher', isLoggedIn,(req,res)=>{
    console.log('voucher : /')
    Voucher.find().exec((err, vouchers)=>{
        // console.log(voucher)
        res.render('modules/voucher/voucher',{vouchers : (vouchers.length > 0 ? vouchers : []), admin : req.session.admin});
    })
})

app.post('/updateVoucher', upload.single('image'), isLoggedIn, async (req,res)=>{
    let voucherId = req.body.voucherId
    console.log('updateVoucher : /')

    let update = {}
    req.body.code ? update.code = req.body.code : ""
    req.body.title ? update.title = req.body.title : ""
    req.body.subtitle ? update.subtitle = req.body.subtitle : ""
    req.body.content ? update.content = req.body.content : ""
    req.body.cost ? update.cost = req.body.cost : ""
    req.body.status ? update.status = req.body.status : ""
    req.body.sortOrder ? update.sortOrder = req.body.sortOrder : ""

    if (!req.file) {
    //   res.status(401).json({error: 'Please provide an image'});

        Voucher.findOneAndUpdate({_id : voucherId},update).exec((err,done)=>{
            if(done){
                res.redirect('/voucher');
            }else{
                console.log(err)
            }
        })
    }else {
        const imagePath = path.join(__dirname, '../assets/img/voucher');
        const fileUpload = new Resize(imagePath);
        const imageSrc = await fileUpload.save(req.file.buffer);
        
        req.file ? update.imageSrc = "http://" + req.headers.host + "/img/voucher/" + imageSrc : ""

        Voucher.findOneAndUpdate({_id : voucherId},update).exec((err,done)=>{
            if(done){
                res.redirect('/voucher');
            }else{
                console.log(err)
            }
        })
    }
})

app.get('/voucherDetails', isLoggedIn, function(req,res,next){
    let voucherId = req.query.voucherId
    Voucher.findById(voucherId).exec((err,voucherDetails)=>{
        res.render('modules/voucher/voucherDetails',{voucher : voucherDetails, admin : req.session.admin});
    })
})

app.get('/deleteVoucher/:voucherId', isLoggedIn, (req,res)=>{
    let voucherId = req.params.voucherId
    console.log('deleteVoucher : /' + voucherId)
    Voucher.remove({_id : voucherId}).exec((err, done)=>{
        if(err){
            console.log(err)
        }
        res.redirect('/voucher');
    })    
})

/* ---------------------------------------------------------setup------------------------------------------------------------- */
app.get('/setup', isLoggedIn, function(req,res,next){
    fbAccountKit.find().exec((err,fbAccountKitAccess)=>{
        MemberId.find().exec((err,memberId)=>{
            res.render('modules/setup/setup',{fbAccountKitAccess : fbAccountKitAccess[0],memberId:memberId[0],admin : req.session.admin})
        })        
    })
})

app.post('/updateSetup', isLoggedIn, function(req,res,next){
    fbAccountKit.findOneAndUpdate({_id : req.body.fbAccountKitAccessId},{authorizationAccessToken : req.body.authorizationAccessToken}).exec((err,done)=>{
        if(err){console.log(err)}
        MemberId.findOneAndUpdate({_id : req.body.memberIdId},{memberId : req.body.memberId}).exec((err,done)=>{
            if(err){console.log(err)}
            res.redirect('/setup')
        })
    })
})

app.get('/logout', function(req,res,next){
    req.session.admin = "";
    res.redirect('/dashboard');
})

module.exports = app;


function isLoggedIn (req, res, next) {
	if (req.session.admin) {
		return next();
	}
	res.redirect('/login');
}

// function notLoggedIn (req, res, next) {
// 	if (!req.session.user) {
// 		return next();
// 	}
// 	res.redirect('/login');
// }