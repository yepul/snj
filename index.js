const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose'); 
const path = require('path');
const fs = require('fs');
const cors = require('cors');
const session = require('express-session')

const app = express(); 
const api  = require('./routes/api/index');
const index = require('./routes/index');
const dashboard  = require('./routes/dashboard/index');
const mongoDB = 'mongodb://localhost/snj';
mongoose.connect(mongoDB, { useNewUrlParser: true });

app.use(session({secret:"epnox@1234", resave:false, saveUninitialized:true}))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'assets')));

// let emailConfig = JSON.parse(fs.readFileSync('./config/emailConfig.json'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const port = process.env.PORT || 8000; 

app.use('/api', api);
app.use('/dashboard', dashboard);
app.use('/', index);
app.listen(port);
console.log("Connected to port : " + port);
